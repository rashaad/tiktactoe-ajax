<?php
	$world=json_decode($_REQUEST['world']);	
	$worldState=$world->{'data'};	

	try {
	  require('constants.inc'); // defines $connection_string	  
	  $conn = new Mongo($connection_string); // open connection to MongoDB server	 
	  $db = $conn->$my_default_db;  // access database	  
	  $collection = $db->$my_collection; // access collection

	    // retrieve existing document 
	  $criteria = array(
	    'world_id' => 'world1',
	  );
	  $doc = $collection->findOne($criteria);
	  if ($doc==null){//if no entry exists, insert 
	  	$collection->insert($world);
	  	}
	  else{
	  	// update document with new values
	  	// save back to collection
	  	$doc['data']=$worldState;
	  	$collection->save($doc);
	  }
	  
	  // disconnect from server
	  $conn->close();
	} catch (MongoConnectionException $e) {
	  die('Error connecting to MongoDB server');
	} catch (MongoException $e) {
	  die('Error: ' . $e->getMessage());
	}

	print json_encode($world);
?>