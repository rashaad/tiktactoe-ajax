<?php

	/*
	e4.php reads the current state of the world from the mongoDB and
	simply returns it. 
	*/

	try {
	  require('constants.inc'); // defines $connection_string	  
	  $conn = new Mongo($connection_string); // open connection to MongoDB server	 
	  $db = $conn->$my_default_db;  // access database	  
	  $collection = $db->$my_collection; // access collection

	    // retrieve existing document 
	  $criteria = array(
	    'world_id' => 'world1',
	  );
	  $doc = $collection->findOne($criteria);

	  // print the data/state of the world
	  print json_encode($doc['data']);
	 	  
	  // disconnect from server
	  $conn->close();
	} catch (MongoConnectionException $e) {
	  die('Error connecting to MongoDB server');
	} catch (MongoException $e) {
	  die('Error: ' . $e->getMessage());
	}
	
?>