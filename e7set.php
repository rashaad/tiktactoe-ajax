<?php

	// defines $connection_string
	require 'constants.inc'; 

	/* Basic CRUD System */
	function create($collection, $worldId){
		try{
			$collection->insert(array('world_id' => trim($worldId)));
		}catch (Exception $e){
			echo "Error Creating {$worldId} : " . $e->getMessage();
		}
	}

	function read($collection, $worldId){
		// conditions to search on
		$criteria = array('world_id' => trim($worldId));
		// retrieve the document
		$doc = $collection->findOne($criteria);
		return $doc;
	}

	function update($collection, $data, $worldId){
		echo "Updating..";
		
		$top = $data['top'];
		$left = $data['left'];
		$id = $data['id'];

		try{
			$collection->update(array("world_id" => $worldId), array('$set' => array("data.{$id}.top" => $top)));
			$collection->update(array("world_id" => $worldId), array('$set' => array("data.{$id}.left" => $left)));
		}catch (Exception $e){
			echo "Error Updating ({$worldId}):".  $e->getMessage();
		}
	}

	//check to make sure we have a valid request
	if(isset($_REQUEST) && isset($_REQUEST['world_id'])){
		echo "<pre>";
		// open connection to MongoDB server
		$conn = new Mongo($connection_string); 
		// access database
		$db = $conn->$my_default_db; 
		// access collection
		$collection = $db->$my_collection;

		$worldId = $_REQUEST['world_id'];

		if(read($collection, $worldId)){
			//update
			update($collection, $_REQUEST, $worldId);
		}else{
			//if the world does not exist
			echo "Creating... " . $worldId;
			create($collection, $worldId);
		}
	}
?>	