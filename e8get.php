<?php
	require 'constants.inc' ; // defines $connection_string

	function getWorlds($collection){
		$doc = $collection->find();
		$worlds = array();
		foreach ($doc as $record) {
		    $worlds[] = $record['world_id'];
		}
		echo json_encode($worlds);
	}

	function getImages($collection, $worldId){
		if(trim($worldId) != ''){
			try{
				$criteria = array('world_id' => trim($worldId));
				$doc = $collection->find($criteria);
				//echo "<pre>";
				$array = iterator_to_array($doc);
				print_r(json_encode($array[key($array)]['images']));
			}catch (Exception $e){
				echo "Error getting images : " . $e->getMessage();
			}	
		}
		
	}

	function read($collection, $worldId){
		// conditions to search on
		$criteria = array('world_id' => trim($worldId));
		// retrieve the document
		$doc = $collection->findOne($criteria);
		return $doc;
	}

	function getConnection($dsn, $db, $my_collection){
		try{
			$conn = new Mongo($dsn); // open connection to MongoDB server    
			$db = $conn->$db;  // access database        
			$collection = $db->$my_collection; // access collection
			return $conn;
		}catch (Exception $e){
			return null;
		}
	}
    /*
    e4.php reads the current state of the world from the mongoDB and
    simply returns it. 
    */

    if(isset($_GET)){
    	
    	if(isset($_GET['getWorlds'])){
    		try {     
    			$conn = getConnection($connection_string, $my_default_db, $my_collection);
				$collection = $conn->$my_default_db->$my_collection;
				getWorlds($collection);
			}catch(Exception $e){
				echo "Error getting worlds : " . $e->getMessage();
			}
    	}elseif(isset($_GET['getImages']) && isset($_GET['world_id'])){
    		$conn = getConnection($connection_string, $my_default_db, $my_collection);
			$collection = $conn->$my_default_db->$my_collection;
    		getImages($collection, trim($_GET['world_id']));
    	}else{
    		try {      
				// retrieve existing document 
				$criteria = array('world_id' => $_GET['world_id']);

				$conn = getConnection($connection_string, $my_default_db, $my_collection);
				
				$doc = $conn->$my_default_db->$my_collection->findOne($criteria);

				$collection = $conn->$my_default_db->$my_collection;

				//read($collection);

				// print the data/state of the world
				echo json_encode($doc['data']);

				// disconnect from server
				$conn->close();
			} catch (MongoConnectionException $e) {
				die('Error connecting to MongoDB server');
			} catch (MongoException $e) {
				die('Error: ' . $e->getMessage());
			}
    	}
    }       
?>
