<?php

	// defines $connection_string
	require 'constants.inc'; 

	function insertImage($collection, $worldId, $image){
		if($worldId != ''){
			$id = $image['id'];
			$url = $image['url'];
			
			try{
				$collection->update(array("world_id" => trim($worldId)), array('$set' => array("data.{$id}.top" => 100)));
				$collection->update(array("world_id" => trim($worldId)), array('$set' => array("data.{$id}.left" => 100)));
				$collection->update(array("world_id" => trim($worldId)), array('$set' => array("images.{$id}.url" => $url)));
				echo json_encode("success");
			}catch (Exception $e){
				echo "Error Updating ({$worldId}):".  $e->getMessage();
			}
		}
	}

	/* Basic CRUD System */
	function create($collection, $worldId){
		if(trim($worldId)){
			try{
				$collection->insert(array('world_id' => trim($worldId)));
				echo json_encode('success');
			}catch (Exception $e){
				echo "Error Creating {$worldId} : " . $e->getMessage();
			}
		}
	}

	function read($collection, $worldId){
		// conditions to search on
		$criteria = array('world_id' => trim($worldId));
		// retrieve the document
		$doc = $collection->findOne($criteria);
		return $doc;
	}

	function update($collection, $data, $worldId){
		
		$top = $data['top'];
		$left = $data['left'];
		$id = $data['id'];

		try{
			$collection->update(array("world_id" => $worldId), array('$set' => array("data.{$id}.top" => $top)));
			$collection->update(array("world_id" => $worldId), array('$set' => array("data.{$id}.left" => $left)));
		}catch (Exception $e){
			echo "Error Updating ({$worldId}):".  $e->getMessage();
		}
	}

	//check to make sure we have a valid request
	if(isset($_POST) && isset($_POST['world_id'])){
		// open connection to MongoDB server
		$conn = new Mongo($connection_string); 
		// access database
		$db = $conn->$my_default_db; 
		// access collection
		$collection = $db->$my_collection;

		$worldId = $_POST['world_id'];

		//check if this is an upload request
		if(isset($_POST['image'])){
			insertImage($collection, $worldId, $_POST['image']);
		}else{
			if(read($collection, $worldId)){
				//update
				update($collection, $_POST, $worldId);
			}else{
				//if the world does not exist
				create($collection, $worldId);
			}
		}
	}
?>	