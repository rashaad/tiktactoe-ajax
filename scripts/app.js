var currentWorld = '';
var interval = null;
var currentHeld = "";

/**
*	Intialize Blocking on the object the user has currently
*	pressed disallowing the other player from moving it
*/
function clickObject(event, ui){
	currentHeld = event.target.id;
	//console.log("Clicked the object");
}

function releaseObject(event, ul){
	//clear the current held since we released it
	//console.log("Releasing " + currentHeld);
	currentHeld = '';
}

function close(){
	window.location.hash = '#close';
}

function clearSelect(element){
	element.find('option').remove().end().append(new Option("Select a world: ", 0));
}

function getWorlds(element){
	////console.log(element);
	$.ajax({
		type: "GET",
		async: true,
		url: "e8get.php",
		data: {'getWorlds':1},
		dataType: "json"
	}).done(function(data){
		if(data == null){return;}
		//console.log("got data");
		clearSelect(element);
		$.each(data, function(key, value){
			$(element).append(new Option(value, value));
		});
	});
}

function selectWorld(element){
	if($(element).val() != '0'){

		currentWorld = $(element).val();
		worldId = $(element).val();
		currentWorld = worldId;

		//show the world status
		$("#worldIn").html("Current World Name: <strong>" + currentWorld + "</strong>").fadeIn(600);
		$("#addImage").fadeIn(600);

		clearInterval(interval);
		interval = setInterval(function(){getWorldState(currentWorld);}, 300);
		
		$("#world").css("opacity", 1);
		//change the target so the modal disappears
		close();
		//reset the world
		resetElements();

		removeUserImages();
		//get the images
		getImages();
	}
}

function createWorld(element){
	if($.trim(element.val()) != ''){
		$.ajax({
			type: "POST",
			async: true,
			url: "e8set.php",
			data: {"world_id":$(element).val()},
			dataType: "json"
		}).success(function(data){//console.log("finished creating world");
			console.log("World created successfully");
			selectWorld(element);

			$("#world").css("opacity", 1);

			element.val("");
		});
	}
}

function createImage(element){
	if($.trim(element.val()) != ''){
		//console.log("creating image");
		image = {};
		image.id = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		image.url = $.trim(element.val());

		$.ajax({
			type: "POST",
			async: true,
			url: "e8set.php",
			data: {"world_id":currentWorld, "image" : image},
			dataType: "json"
		}).success(function(data){
			if(data == null){return;}
			//console.log("Finished Sending Image..");
			removeUserImages();
			getImages();
		});
	}
}

function removeUserImages(){
	//console.log("Removing images");
	$(".user-image").remove();
}

function getImages(){
	$.ajax({
		type: "GET",
		async: true,
		url: "e8get.php",
		data: {"getImages":1, "world_id":$.trim(currentWorld)},
		dataType: "json"
	}).success(function(data){
		if(data == null){return;}
	  	var ids = [];
	  	$.each(data, function(id, src){
	  		ids.push(id);
	  		if($("#"+id).length <= 0){
	  			var img = new Image();
		  		img.src = src.url;
		  		img.id = id;
		  		img.className = "draggable ui-draggable user-image";
		  		$(img).css('z-index', 2);
		  		$("#world").append(img);
	  		}
	  	});


	  	console.log(ids);

	  	$(".user-image").each(function(k,v){
	  		
	  		if($.inArray(v.id, ids) < 0){
	  			console.log("Removing invalid image");
	  			$(this).remove();
	  		}
	  	});
	  	
	  	init();
	});
}

/**
*	Handler that will send an update request that will update 
*	the db on the objects's new position after it has finshed
* 	animating
*/
function finishDrag(event, ui){
	//currentHeld = event.target.id;
	//console.log("Running Update...");
	state = {};
	state.world_id = currentWorld;
	state.id = event.target.id;
	state.left = event.target.style.left;
	state.top = event.target.style.top;

	$.ajax({
		type: "POST",
		async: true,
		url: "e8set.php",
		data: state,
		dataType: "json"
	}).done(function(msg){
		//console.log("Finished Update..");
	}).always(function(){
		//console.log("Restarting Interval..");
		currentHeld = '';
		clearInterval(interval);
		startInterval();
	});
}

function resetElements(){
	$("#world img").each(function(data){
		$(this).stop().animate({
			top: ($(window).height() / 2) - ($(this).height() / 2),
			left: ($(window).width() /2) - ($(this).width() / 2)
		}, 100, 'easeOutCirc');
	});
}

function startInterval(){
	interval = setInterval(function(){getWorldState(currentWorld);}, 400);
}

/**	 
*	Continually requests the current state of
*	the world from e7get.php and updates the positions of the items in the
*	world. 
*/
function getWorldState(worldId){

	//get the images on each request
	getImages();
	
	$.ajax({
		type: "GET",
		async: true,
		url: "e8get.php",
		data: {"world_id":worldId},
		dataType: "json"
	}).done(function(data){
		if(data == null){return;}
		//parse the data
		if(data != null){
			$.each(data, function(key, value) {						
				if(currentHeld!=key){ //only update objects that we are not currently dragging							
					$("#"+key).stop().animate({
						top: this.top,
						left: this.left
					}, 100, 'easeOutCirc');
				}else{
					//console.log("Skipping " + key);
				}
			});
		}else{
			//console.log("resetting items");
			resetElements();
		}
		
	}).fail(function(data){
		//console.log("you fail my friend!");
	});
}

function init(){
	//console.log("Intializing elements");
	$("#world img").draggable({
		containment: [10, 50, Math.round($(window).width()-50), Math.round($(window).height())],
		helper: function(){
			return $('<div></div>').css('opacity',0);
		},
		start: function(event, ui){
			clickObject(event);
			//console.log('starting drag...');
		},
		stop: function(event, ui){
			//console.log('stopping drag');
			clearInterval(interval);
		},
		drag: function(event, ui){
			//stop any animation in progress
			//and animate to the UI helper position
			//i.e. the place where your cursor is

			currentHeld = event.target.id;
			$(this).stop().animate({
				top: ui.helper.position().top,
				left: ui.helper.position().left
			}, 100, 'easeOutCirc', function(){
				//console.log("item animating: " + event.target.id);
				//callback indicating that the drag animation completed
				finishDrag(event, ui);
			});
		}
	}); 
}

$(function() {
	/*	 
		When a user drags an image, use Ajax to send only the element that has changed to
		e7set.php which will then save that state.
	*/
	
	/**
	*	Setup the objects so they can drag with easing
	*	This means we also need to update the server 
	*	once the animation has completed. 
	*/

	//display the worlds modal
	window.location.hash = '#worlds';

	//display the world
	$("#world").css("display", "inline-block");

	//hide world status
	$("#worldIn").hide();
	$("#addImage").hide();
	init();
});